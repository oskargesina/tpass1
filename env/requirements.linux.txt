# Packages from WHL's

# ...

# Packages from PIP

mysqlclient==1.4.2
uwsgi~=2.0.0

# Packages from GIT

git+https://gitlab.com/integree/internal/congo3.git
