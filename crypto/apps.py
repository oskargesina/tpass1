from django.apps import AppConfig


class PgpConfig(AppConfig):
    name = 'crypto'
