import base64

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from django.http.response import HttpResponse


def gen_keys(request):

    # passphrase
    passphrase = None

    # private_key
    private_key = RSA.generate(2048)
    private_key_pem = private_key.export_key(passphrase = passphrase, pkcs = 8, protection = "scryptAndAES128-CBC")
    utf8_private_key = private_key_pem.decode()

    print('private_key', private_key)
    print('private_key_pem', private_key_pem)
    print('utf8_private_key', utf8_private_key)

    # public_key
    public_key = private_key.publickey()
    public_key_pem = public_key.export_key()
    utf8_public_key = public_key_pem.decode()

    print('public_key', public_key)
    print('public_key_pem', public_key_pem)
    print('utf8_public_key', utf8_public_key)

    return HttpResponse('OK')

# -----BEGIN ENCRYPTED PRIVATE KEY-----
# MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCyIPsoXps0qi2H
# ecP7vbuBs4z8kU5/WmQ7yIQj8GRQyvZj2/NGZhtJI8Ypl/Rplq79eRk8NrsP9c+h
# HUhM6PO8fvHV3541QyRqudVVCZjKXmaxjgQ5jccGlK2wPTrwPlzJFqPfTdOuoyNl
# BFeG1yx3puCCMDu7LSHD16tAO5aWPOWrtWrnHECuGC1vQrDitb//JiVR1nlqaHrf
# iTzZ/CUNfG6x5gzJ6nOyZA3+3jP/qKM7gHdHkg/a0FSNuiIs8AtW8TWLAgT/Tuk0
# DNe9sOS19Ma3lE9CNqrFCslgZs7BnqIkOCGzTsz/r07iKm2zz7b9E1x6IoQKuGkA
# xbwZbZEzAgMBAAECggEAOXs8ITxaUGXkOFYaYNfKtOGYiv5m+uDT/JjJz4EpFgtQ
# 9EuLlzmm73S8Yzq2dAZcpM4Zzx0IQ2+6106IQM1hrf+3EaHOP3jRvSEHzx/ALvnE
# oOJnyZzPT3fWDsvLg7BUJ84HBq3qM+5QIy6CAjDYmCoBQ/HFrLK1NSoZCKrR/tvQ
# /PzoM3On+s3LIh5pg+vZCMB7pR7VPIa6SaqYVb1S+deUWOLPaC4vcwZComgRHIEY
# j8UOBLg/R2m8MITjI5dBtrfSl/kEhhtMvDlNThd8hgUK3NAG1GjJzyKnKY8OwHO6
# kNTNa8/67PZebEn3R8UThJ4z7/7rPSearqHJl6urTQKBgQDMmP2zzTFK4zJETgD0
# EZ6+uQaq2gpmYn5EqaqhUJJsXwZ2IejsBShObQ7f4hGkuzErhW542kNEyo4DbA1h
# 5KygF9y4AC/BbGqHnqTlw05NzjGTDWP/R72T7mkWqmHhBx6BVKZCz68duoMfg2GR
# aF9/s1qVfcnuzIfPlfj6eagmBQKBgQDe4ZwCzQFRlAqhrpMa3I+Wl6lE+zbK7IKH
# mU52cYEApiZwilV6W12762cvKFUjhWUMm9+5kKgoUUrWZzXZWgvzYM9RwV7hW/+m
# OudJxqnbBPb4QX0qIJJZdWGqbcAe6ReoJILmYXSFlncjy8viysoY5XXhBuc5WQ1u
# N9OZU9WH1wKBgQCcHw5RGn3HsFCWMJOnqp5g2N2Ol+0hrXy+tl0+1TW2Pli24i9T
# kqfsGGomSuK9uh3OIPZof9ye7JWrZNNdZNqZGzxQp8vJJAqyTEt2DC9/TZ0koi4x
# CTRPYriMFB22UfFkLQzlpIby/3S5Ik7cXzGoR1sTAbaKY0YrEDy6HQVDZQKBgGWQ
# GS+Ni5RJCa8zhgUvjJ7q7U9pykYeED0KNx7yCf7pt4qA/wKKr+P6GUL79hABsVJV
# lVVR/c6hfKDG11r8FYYKD28J1tqKuaxeGxcbWDtdv8zQyuBvh0EMpDytVma/gPA1
# 8xBYSGi3cAL30yi9j+pYZabtJmCTm/MlrV6pPRxxAoGBAIb6tSA1Z48ltEW2BV5s
# 2RLb2sOXP+pYOv+wq6IvVpL87cZP7XVuCCmU2b0jbdsRRhALZ1A9qRNmw7kjGJzQ
# +WrI+1FaPnOF+C5EIuGyn+CoICmfPFARnIRWMF9Be7YndYuRhkaxdKKqcDDEaMxG
# MX60O6Ld6I5DgL9ZuEUAciPT
# -----END ENCRYPTED PRIVATE KEY-----

# -----BEGIN PUBLIC KEY-----
# MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsiD7KF6bNKoth3nD+727
# gbOM/JFOf1pkO8iEI/BkUMr2Y9vzRmYbSSPGKZf0aZau/XkZPDa7D/XPoR1ITOjz
# vH7x1d+eNUMkarnVVQmYyl5msY4EOY3HBpStsD068D5cyRaj303TrqMjZQRXhtcs
# d6bggjA7uy0hw9erQDuWljzlq7Vq5xxArhgtb0Kw4rW//yYlUdZ5amh634k82fwl
# DXxuseYMyepzsmQN/t4z/6ijO4B3R5IP2tBUjboiLPALVvE1iwIE/07pNAzXvbDk
# tfTGt5RPQjaqxQrJYGbOwZ6iJDghs07M/69O4ipts8+2/RNceiKECrhpAMW8GW2R
# MwIDAQAB
# -----END PUBLIC KEY-----


def encrypt(request):

    # utf8_public_key
    utf8_public_key = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsiD7KF6bNKoth3nD+727
gbOM/JFOf1pkO8iEI/BkUMr2Y9vzRmYbSSPGKZf0aZau/XkZPDa7D/XPoR1ITOjz
vH7x1d+eNUMkarnVVQmYyl5msY4EOY3HBpStsD068D5cyRaj303TrqMjZQRXhtcs
d6bggjA7uy0hw9erQDuWljzlq7Vq5xxArhgtb0Kw4rW//yYlUdZ5amh634k82fwl
DXxuseYMyepzsmQN/t4z/6ijO4B3R5IP2tBUjboiLPALVvE1iwIE/07pNAzXvbDk
tfTGt5RPQjaqxQrJYGbOwZ6iJDghs07M/69O4ipts8+2/RNceiKECrhpAMW8GW2R
MwIDAQAB
-----END PUBLIC KEY-----"""

    # public_key
    public_key_pem = utf8_public_key.encode()
    public_key = RSA.import_key(public_key_pem)

    print('utf8_public_key', utf8_public_key)
    print('public_key_pem', public_key_pem)
    print('public_key', public_key)

    # message
    message = request.GET.get('msg', "This is my secret message: Puknąłbym kul3czkę Lotto")
    byte_message = message.encode('utf8')

#     cipher = PKCS1_OAEP.new(public_key)
#     encrypted_message = cipher.encrypt(byte_message)
#     base64_encrypted_message = b2a_base64(encrypted_message)

    cipher = PKCS1_v1_5.new(public_key)
    encrypted_message = cipher.encrypt(byte_message)
    base64_encrypted_message = base64.b64encode(encrypted_message).decode('ascii')

    print('message', message)
    print('byte_message', byte_message)
    print('encrypted_message', encrypted_message)
    print('base64_encrypted_message', base64_encrypted_message)

    return HttpResponse('OK')

# z haslem
# b'\xc0\x8a\xfe\xd7\'1\xed\x9a\x94\xab\xd8Y\x96\x08u\xe2\xc1\x07\xdc\x96\x02\x8e|\xbe\xae\xff\xfd\x05\xbd\xb7c\xa3\x96{Z\x9cf\xadB\x87\xf9K\xd1F2\xd5\'\xba\x16\xed \x03r\xda\x7f,;q\xca\x8cP\xe6fG\xdb7\x8b\x89\x92\x9f\'\xb1\x11%Z\x18\xe0\xb3E\x88z\x99\xed\x96\x96_=E\xd0i\x19\x9e\xa5\xcd\x97\x0b\xf5\x135\xb6\x7fz\xfa\xcc\x80V\x83\xe5\xa8\x8fRv\x0ey\xd2H\xd7\xc6C\xe6g,\x0b\x8d\x1c]JI^\xcd\xa6$T86\x18\xe7u\xbd\xfa">\xbb\xe4g\xceR\x01\xac\xfa"\xe3\xc5,D\xf0\xe9\x8b\xa1\r\x08\x19n\xc6\xb9\x8d\x19\xee\xfe\x9eY\x03\xa9j\xa4?\xa7\xa8\xfc\xfdE\xcb\xa8q\xcf\xb5;\x11\xe9\xcb\xe0P|t\xe1\x9c\xab\xc8\x9d\x89\xe5q\xf1>\xffT\xed\xf3-\x06P\xce\xf2u\xc8\xfe\x1b\x9a:@\xe8\r\x0bT\xf0\xee\x18\xa5\xa25O\xbe\xe6\xe9!\x89\xa4lE\n\xa0Jx\xf6X\xdcFCZ\xe1\xa3\xa2=\x0e\xc1g'
# b'wIr+1ycx7ZqUq9hZlgh14sEH3JYCjny+rv/9Bb23Y6OWe1qcZq1Ch/lL0UYy1Se6Fu0gA3Lafyw7ccqMUOZmR9s3i4mSnyexESVaGOCzRYh6me2Wll89RdBpGZ6lzZcL9RM1tn96+syAVoPlqI9Sdg550kjXxkPmZywLjRxdSklezaYkVDg2GOd1vfoiPrvkZ85SAaz6IuPFLETw6YuhDQgZbsa5jRnu/p5ZA6lqpD+nqPz9Rcuocc+1OxHpy+BQfHThnKvInYnlcfE+/1Tt8y0GUM7ydcj+G5o6QOgNC1Tw7hilojVPvubpIYmkbEUKoEp49ljcRkNa4aOiPQ7BZw==\n'

# bez hasla
# b'E\xfe\xa0\xad\x98_\xd9\xf1\x8b\x99\xb7\x9e\xce\xc5A-7\xcbX>h\x0fR\xee\xa6\xf7\xa5\x0el8\xb6)\xf7\xda\x9f\xb3\xd9\xa3"\x9f\x97\xcf\x9f\xdf\x03\x89\x00\xe2\xb4\xc1\\\x92\x93\xc7\xf7V\x0f\x94\xa0y\xa4\x1byZC\xf8#\x02\x8aU\x18 \x1a\x9b\xa5\xeeb\x1c\xa7$\xb3\xbd\xad\xfaF,\xef\x10\xcb\x060\xad\xdc\xb4\xa4\xf4\xadz\xa2\x1eh\x84W\xb2\xc5\x81\x99\xd1\xcc\xbe7\xdc\xd7\x11\xd6\xf7\x95\x96\x1c\x83mu\xaeyl\x92\x8aB\xd8.\xb8$\xfe\xd6\x9b\xab\xb8\xb1\xa3\x00\x86\x95\xf4\x81\x96\xf1}\xd4\x9e\xee$\x00\x89\xcf\x1d\x95\x16\x93\xcc\xdd\xcd\xdd:x$\x9a\x9c\x03H\x93*U\xd6\x82\x0e\xa7\xb4\xadEN\xdf\xb5\xc3$\nN\xfb\xb2\xe3RBj\x9a\x04\x93I5\x80\x8d\xaf\xd7V\'\x91\xbb\x07#\x17"\xc4\xee\xab\x0f\x9cIz\xcc\xad\xa2=G\xf2Y\x84\xa1\x06T\xde 3\x80\x04\xc3Z\x07OR\xbel?p\xedO\xd1\x96\xe4\x8d\xe6Rv\x9b1\xb6\t\xff\xad'
# Rf6grZhf2fGLmbeezsVBLTfLWD5oD1LupvelDmw4tin32p+z2aMin5fPn98DiQDitMFckpPH91YPlKB5pBt5WkP4IwKKVRggGpul7mIcpySzva36RizvEMsGMK3ctKT0rXqiHmiEV7LFgZnRzL433NcR1veVlhyDbXWueWySikLYLrgk/tabq7ixowCGlfSBlvF91J7uJACJzx2VFpPM3c3dOngkmpwDSJMqVdaCDqe0rUVO37XDJApO+7LjUkJqmgSTSTWAja/XVieRuwcjFyLE7qsPnEl6zK2iPUfyWYShBlTeIDOABMNaB09Svmw/cO1P0ZbkjeZSdpsxtgn/rQ==


def decrypt(request):

    # passphrase
    passphrase = None

    # utf8_private_key
    utf8_private_key = """-----BEGIN ENCRYPTED PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCyIPsoXps0qi2H
ecP7vbuBs4z8kU5/WmQ7yIQj8GRQyvZj2/NGZhtJI8Ypl/Rplq79eRk8NrsP9c+h
HUhM6PO8fvHV3541QyRqudVVCZjKXmaxjgQ5jccGlK2wPTrwPlzJFqPfTdOuoyNl
BFeG1yx3puCCMDu7LSHD16tAO5aWPOWrtWrnHECuGC1vQrDitb//JiVR1nlqaHrf
iTzZ/CUNfG6x5gzJ6nOyZA3+3jP/qKM7gHdHkg/a0FSNuiIs8AtW8TWLAgT/Tuk0
DNe9sOS19Ma3lE9CNqrFCslgZs7BnqIkOCGzTsz/r07iKm2zz7b9E1x6IoQKuGkA
xbwZbZEzAgMBAAECggEAOXs8ITxaUGXkOFYaYNfKtOGYiv5m+uDT/JjJz4EpFgtQ
9EuLlzmm73S8Yzq2dAZcpM4Zzx0IQ2+6106IQM1hrf+3EaHOP3jRvSEHzx/ALvnE
oOJnyZzPT3fWDsvLg7BUJ84HBq3qM+5QIy6CAjDYmCoBQ/HFrLK1NSoZCKrR/tvQ
/PzoM3On+s3LIh5pg+vZCMB7pR7VPIa6SaqYVb1S+deUWOLPaC4vcwZComgRHIEY
j8UOBLg/R2m8MITjI5dBtrfSl/kEhhtMvDlNThd8hgUK3NAG1GjJzyKnKY8OwHO6
kNTNa8/67PZebEn3R8UThJ4z7/7rPSearqHJl6urTQKBgQDMmP2zzTFK4zJETgD0
EZ6+uQaq2gpmYn5EqaqhUJJsXwZ2IejsBShObQ7f4hGkuzErhW542kNEyo4DbA1h
5KygF9y4AC/BbGqHnqTlw05NzjGTDWP/R72T7mkWqmHhBx6BVKZCz68duoMfg2GR
aF9/s1qVfcnuzIfPlfj6eagmBQKBgQDe4ZwCzQFRlAqhrpMa3I+Wl6lE+zbK7IKH
mU52cYEApiZwilV6W12762cvKFUjhWUMm9+5kKgoUUrWZzXZWgvzYM9RwV7hW/+m
OudJxqnbBPb4QX0qIJJZdWGqbcAe6ReoJILmYXSFlncjy8viysoY5XXhBuc5WQ1u
N9OZU9WH1wKBgQCcHw5RGn3HsFCWMJOnqp5g2N2Ol+0hrXy+tl0+1TW2Pli24i9T
kqfsGGomSuK9uh3OIPZof9ye7JWrZNNdZNqZGzxQp8vJJAqyTEt2DC9/TZ0koi4x
CTRPYriMFB22UfFkLQzlpIby/3S5Ik7cXzGoR1sTAbaKY0YrEDy6HQVDZQKBgGWQ
GS+Ni5RJCa8zhgUvjJ7q7U9pykYeED0KNx7yCf7pt4qA/wKKr+P6GUL79hABsVJV
lVVR/c6hfKDG11r8FYYKD28J1tqKuaxeGxcbWDtdv8zQyuBvh0EMpDytVma/gPA1
8xBYSGi3cAL30yi9j+pYZabtJmCTm/MlrV6pPRxxAoGBAIb6tSA1Z48ltEW2BV5s
2RLb2sOXP+pYOv+wq6IvVpL87cZP7XVuCCmU2b0jbdsRRhALZ1A9qRNmw7kjGJzQ
+WrI+1FaPnOF+C5EIuGyn+CoICmfPFARnIRWMF9Be7YndYuRhkaxdKKqcDDEaMxG
MX60O6Ld6I5DgL9ZuEUAciPT
-----END ENCRYPTED PRIVATE KEY-----"""

    # private_key_pem
    private_key_pem = utf8_private_key.encode()

    # private_key
    private_key = RSA.import_key(private_key_pem, passphrase)

    # message
    base64_encrypted_message = 'Rf6grZhf2fGLmbeezsVBLTfLWD5oD1LupvelDmw4tin32p+z2aMin5fPn98DiQDitMFckpPH91YPlKB5pBt5WkP4IwKKVRggGpul7mIcpySzva36RizvEMsGMK3ctKT0rXqiHmiEV7LFgZnRzL433NcR1veVlhyDbXWueWySikLYLrgk/tabq7ixowCGlfSBlvF91J7uJACJzx2VFpPM3c3dOngkmpwDSJMqVdaCDqe0rUVO37XDJApO+7LjUkJqmgSTSTWAja/XVieRuwcjFyLE7qsPnEl6zK2iPUfyWYShBlTeIDOABMNaB09Svmw/cO1P0ZbkjeZSdpsxtgn/rQ=='
    encrypted_message = base64.b64decode(base64_encrypted_message.encode('ascii'))

    cipher = PKCS1_v1_5.new(private_key)
    byte_message = cipher.decrypt(encrypted_message, b'DECRYPTION FAILED')
    message = byte_message.decode('utf8')

#     encrypted_message = "ijG7tutZL4whkCcWy1JPxTkp5TbxoxO/AqcnnmFjcgR6ok424BqXYJdaM1b+jkbbQsM3ih7CE09Sxp9pXEidAqbdQJOpu6GNW8Bf1DRRPs13iR25M7J78eW7YhFsz6vjUHUCPQIIp8uo9Wf3JYK/KgnVCykxjyUGl2577cfzMdi3DDkaZZKjJ4OqJkszGCAKcTsrxFB1qEztHwH4jfaHF3U+TyW8Dd6+BiazjUvORk1whmmaBBcqQ/KTHlNEbognf1FMsS3Qqipq3mQbymABqEUsOyz3b49JD0ILBcP+IGQ5m7LkaQ9NtO/gU4Y+yD8KsfMYHN+DY/hstsR2rgFiKg=="
#
#     cipher = PKCS1_OAEP.new(private_key)
#     byte_message = cipher.decrypt(encrypted_message)
#     message = byte_message.decode()

    print('base64_encrypted_message', base64_encrypted_message)
    print('encrypted_message', encrypted_message)
    print('byte_message', byte_message)
    print('message', message)

    return HttpResponse('OK')
