# -*- coding: utf-8 -*-
import logging
import traceback

from django.utils.translation import gettext_lazy as _


def get_message(message_id, app_name = None):
    try:
        message = MESSAGES[message_id]

    except KeyError:
        message = message_id

        extra = {'extra_info': traceback.format_exc()}
        logger = logging.getLogger('django.core')
        logger.log(Log.ERROR, "Brak wiadomości o ID '%s'" % message_id, extra = extra)

    return message


MESSAGES = {

    # Validation errors
    'invalid_date_user_absence' : _("Podana data jest nieprawidłowa."),
    'invalid_value_gift_card' : _("Podana wartość jest nieprawidłowa."),
    'required_field' : _("To pole jest wymagane."),
    'required_serial_no' : _("To pole jest wymagane. Jeśli nie posiadasz numeru seryjnego urządzenia zgłoś szkodę telefonicznie pod numerem 71 72 70 246"),
    'invalid_policy_no': _("Podaj poprawną wartość numeru polisy, np. 12345.1234567"),
    'incompatible_email' : _("Podane adresy email różnią się od siebie."),
    'invalid_value': _("Niepoprawna wartość. Podaj wartość większą od 0."),
    'invalid_date': _('Podana data jest nieprawidłowa. Podaj wcześniejszą datę.'),
    'invalid_date_future' : _('Podana data jest nieprawidłowa. Podaj późniejszą datę.'),
    'invalid_date_insurance' : _('Data musi być późniejsza od daty zakupu.'),
    'invalid_date_end_insurance' : _('Data zakończenia musi być późniejsza od daty rozpoczęcia.'),
    'invalid_date_responsibility': _('Okres odpowiedzialności musi być późniejsza od okresu ubezpieczenia.'),
    'invalid_date_responsibility_end': _('Data musi być wcześniejsza od daty zakończenia.'),
    'invalid_date_insurance_res': _('Data musi być wcześniejsza od daty zakończenia odresu ubezpieczenia.'),
    'invalid_phone': _('Podaj poprawny numer telefonu.'),
    'invalid_zip_code': _('Podaj poprawny kod pocztowy, np. 00-000.'),
    'invalid_damage_date': _('Data nie może byc wsześniejsza niż data zdarzenia.'),
    'invalid_purchase_date': _('Data nie może być wcześniejsza od daty zakupu'),
    'invalid_theft_date': _('Data musi być wcześniejsza od daty zgłoszenia na polcję.'),
    'blocked_uid': _("Szanowni Państwo,<br>Uprzejmie informujemy, iż zgłoszenia szkody z ubezpieczenia zakupionego przed 1.07.2019r. można dokonać w następujący sposób:<br>1. wysyłając wiadomość elektroniczną na adres e-mail: ubezpieczenia@spb.eu, lub<br>2. dzwoniąc pod numer telefonu 22 438 44 44.<br>Z poważaniem,<br>Zespół SPB Polska"),
    'invalid_position_value': _("Podana wartość jest nieprawidłowa, Wpisz odpowiednią kwotę lub *."),

    # Admin Activity
    'admin_created' : _("Utworzono %(verbose_name)s"),
    'admin_changed' : _("Zmieniono %(verbose_name)s"),
    'admin_deleted' : _("Usunięto %(verbose_name)s"),

    # Account
    'account_password_reset_no_email_found': _("Ten adres e-mail nie ma skojarzonego konta użytkownika. Czy jesteś pewien, że jesteś zarejestrowany?"),
    'user_created': _("Użytkownik został dodany."),
    'user_changed': _("Dane użytkownika %(name)s zostały zmienione"),
    'log_user_changed': _("Dane użytkownika zostały zmienione"),
    'log_account_seen': _("Dane użytkownika %(name)s zostały wyświetlone przez %(user)s"),
    'log_account_logged_in': _("Zalogowano"),
    'log_account_login_failed': _("Nieudana próba logowania"),
    'log_account_login_failed_unknown_email': _("Nieudana próba logowania na adres e-mail '%(email)s'"),
    'log_account_logged_out': _("Wylogowano"),
    'log_account_password_changed': _("Zmieniono hasło"),
    'log_account_password_change_failed': _("Nieudana próba zmiany hasła"),
    'log_account_password_reset': _("Wysłano maila resetującego hasło"),
    'log_account_password_reset_failed': _("Nieudana próba resety hasła na adres e-mail '%(email)s'"),
    'log_account_password_reset_confirm': _("Zresetowano hasło"),
    'account_password_reset_account_blocked': _("Niestety nie możesz zmienić hasła – konto zostało zablokowane. Spróbuj ponownie za jakiś czas."),
    'log_permission_group_added': _("Dodano grupe uprawnień."),
    'parmission_group_added': _("Dodano grupe uprawnień."),
    'log_permission_group_changed': _("Zmieniono grupe uprawnień."),
    'permission_group_changed': _("Grupa uprawnień %(name)s została zmieniona"),
    'permission_group_has_users': _("Ta grupa uprawnień ma przypisanych użytkowników."),
    'log_permission_group_deleted': _("Grupa uprawnień została usunięta."),
    'log_user_created': _("Użytkownik został dodany."),
    'permission_group_deleted': _("Grupa uprawnień %(name)s została usunięta."),
    'log_permission_group_seen': _("Szegóły grupy uprawnień %(name)s zostały wyświetlone przez %(user)s"),
    'password_expired': _("Twoje hasło wygasło. Wprowadź nowe"),
}
