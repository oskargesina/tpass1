from django.contrib.auth.models import AnonymousUser
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from accounts.authentication import get_token, is_token_expired


class BaseViewSet(object):
    # @FG token

    def initialize_request(self, request, *args, **kwargs):
        request = super(BaseViewSet, self).initialize_request(request, *args, **kwargs)
        authToken =  request.META.get('HTTP_AUTHORIZATION')

        self.token = None

        if authToken:
            token = get_token(authToken)
            if token and not is_token_expired(token):
                request.user = token.user
                self.token = token
                # TODO: dodac pole expire_date

            else:
                request.user = AnonymousUser()
        else:
            request.user = AnonymousUser()

        return request

    def get_permissions(self):
        return (IsAuthenticated(),)
