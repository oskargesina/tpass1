from congo.accounts import admin as congo_admin

from django.contrib import admin
from django.contrib.auth.models import Group as DjnagoGroup
from accounts.models import User, UserConfig
# Register your models here.


class UserAdmin(congo_admin.UserAdmin):
    list_display = ('get_full_name', 'email', 'is_staff', 'is_active')
    list_filter = ('is_staff', 'is_active', 'groups')
    readonly_fields = ('date_joined', 'last_login')

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ("Dane osobiste", {'fields': ('first_name', 'last_name', 'mobile_phone')}),
        ("Uprawnienia", {'fields': ('is_active', 'is_staff',)}),
        ("Inne informacje", {'fields': ('date_joined', 'last_login',)}),
    )


class UserConfigAdmin(congo_admin.UserConfigAdmin):
    pass



admin.site.unregister(DjnagoGroup)
admin.site.register(User, UserAdmin)
admin.site.register(UserConfig, UserConfigAdmin)
