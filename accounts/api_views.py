from django.contrib.auth import login as dj_login, logout as dj_logout
from django.db import transaction
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from accounts.authentication import check_token_or_refresh
from accounts.forms import AuthenticationForm, RegistrationForm
from accounts.models import User
from accounts.serializers import UserSerializer
from core.api_views import BaseViewSet
from core.messages import get_message


class UserViewSet(BaseViewSet, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        if self.action in ('register', 'login'):
            # widoki register i login dostepne dla userow anoniowych
            return (AllowAny(),)

        return (IsAuthenticated(),)

    @action(methods = ["post"], detail = False, url_path = 'login')
    def login(self, request, *args, **kwargs):
        data = request.data

        form = AuthenticationForm(request, {
            'username': data.get('username'),
            'password': data.get('password'),
        })

        result = {}

        if form.is_valid():
            dj_login(request, form.user_cache)
            token, created = Token.objects.get_or_create(user = request.user)
            is_expired, token = check_token_or_refresh(token)
            result["token"] = token.key

            result['message'] = get_message('log_account_logged_in')
            return Response(result, HTTP_200_OK)

        else:
            result = form.errors
            return Response(result, HTTP_400_BAD_REQUEST)

    @transaction.atomic
    @action(methods = ["post"], detail = False, url_path = 'logout')
    def logout(self, request, *args, **kwargs):
        result = {}

        Token.objects.filter(key = self.token, user = request.user).delete()
        dj_logout(request)

        result['message'] = get_message('log_account_logged_out')
        return Response(result, HTTP_200_OK)

    @action(methods = ["post"], detail = False, url_path = "register")
    def register(self, request, *args, **kwargs):
        data = request.data

        form = RegistrationForm({
            'first_name': data.get('first_name'),
            'last_name': data.get('last_name'),
            'email': data.get('email'),
            'password1': data.get('password1'),
            'password2': data.get('password2'),
        })

        result = {}

        if form.is_valid():

            private_key, public_key = User.generate_keys()

            user = form.save()
            user.public_key = public_key
            user.is_active = True
            user.save(update_fields=['public_key', 'is_active', ])

            dj_login(request, user)
            token, created = Token.objects.get_or_create(user = request.user)
            is_expired, token = check_token_or_refresh(token)

            result["token"] = token.key
            result['private_key'] = private_key
            result['user_id'] = user.id

            return Response(result, HTTP_200_OK)
        else:

            result = form.errors
            return Response(result, HTTP_400_BAD_REQUEST)

    @action(methods = ["get"], detail = False, url_path = "get-user")
    def get_user(self, request, *args, **kwargs):

        user = request.user

        result = {
            "user_id": user.id,
            "first_name": user.first_name,
            "last_name": user.last_name
        }

        return Response(result, HTTP_200_OK)
