# -*- coding: utf-8 -*-
from congo.accounts.forms import AuthenticationForm as CongoAuthenticationForm, PasswordResetForm as CongoPasswordResetForm, RegistrationForm as CongoRegistrationForm

# from dal import autocomplete
from django import forms
from django.utils import timezone

from accounts.models import User
from core.messages import get_message


class AuthenticationForm(CongoAuthenticationForm):

    def confirm_login_allowed(self, user):
#        if user.block_date:
#            raise forms.ValidationError(get_message('account_blocked'), code = 'blocked')
        if not user.is_active:
            raise forms.ValidationError(get_message('account_inactive'), code = 'inactive')

class RegistrationForm(CongoRegistrationForm):
    pass


class PasswordResetForm(CongoPasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)

    def clean_email(self):
        email = self.cleaned_data.get("email")

        self.users_cache = User.objects.filter(email = email, is_active = True)
        if not len(self.users_cache):
            raise forms.ValidationError(get_message('account_password_reset_no_email_found'))
        if User.objects.filter(email = email, block_date__gt = timezone.now()).exists():
            raise forms.ValidationError(get_message('account_password_reset_account_blocked'))
        return email

    def save(self, *args, **kwargs):
        email = self.cleaned_data["email"]
        for user in self.get_users(email):

            user.send_password_reset_email()


# class PermissionGroupPermissionForm(forms.ModelForm):

#     class Meta:
#         model = PermissionGroup.permissions.through
#         fields = '__all__'
#         widgets = {
#             'permissions': autocomplete.ModelSelect2(url = 'admin_permission_autocomplete')
#         }

