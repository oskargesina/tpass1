from datetime import timedelta

from django.conf import settings
from django.utils import timezone

from rest_framework.authtoken.models import Token

def get_token(key):
    token = None

    try:
        token = Token.objects.get(key = key)
    except Token.DoesNotExist:
        pass

    return token


# pozostaly czas do wygasniecia token'a
def expires_in(token):
    time_elapsed = timezone.now() - token.created
    left_time = timedelta(seconds = settings.TOKEN_EXPIRED_AFTER_SECONDS) - time_elapsed
    return left_time

# sprawdzamy czy token wygasl
def is_token_expired(token):
    return expires_in(token) < timedelta(seconds = 0)

# sprawdzamy czy token wygasl, jesli tak stary usuwamy i tworzymy nowy
def check_token_or_refresh(token):
    is_expired = is_token_expired(token)
    if is_expired:
        token.delete()
        token = Token.objects.create(user = token.user)
    return is_expired, token
