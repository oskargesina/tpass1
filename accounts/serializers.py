# -*- coding: utf-8 -*-
import re
from django.utils import formats
from rest_framework import serializers

from accounts.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
        )