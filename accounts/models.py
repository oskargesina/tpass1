from base64 import b64encode
from congo.accounts import models as congo_models
from congo.accounts.models import AbstractUser

from Crypto.Cipher import PKCS1_v1_5
from Crypto.PublicKey import RSA
from django.db import models


class UserManager(congo_models.AbstractUserManager):
    pass


class User(AbstractUser):
    public_key = models.TextField("RSA public key")

    def get_cipher(self):
        utf8_public_key = self.public_key
        public_key_pem = utf8_public_key.encode()
        public_key = RSA.import_key(public_key_pem)
        cipher = PKCS1_v1_5.new(public_key)

        return cipher

    def encrypt(self, phrase, cipher = None):
        if cipher is None:
            cipher = self.get_cipher()

        byte_phrase = phrase.encode('utf8')
        encrypted_phrase = cipher.encrypt(byte_phrase)
        base64_phrase = b64encode(encrypted_phrase).decode('ascii')

        return base64_phrase

    @classmethod
    def generate_keys(cls):
        # TODO: tworzyć klucz zabezpieczony hasłem
        passphrase = None

        private_key = RSA.generate(2048)
        private_key_pem = private_key.export_key(passphrase = passphrase, pkcs = 8, protection = "scryptAndAES128-CBC")
        utf8_private_key = private_key_pem.decode('utf8')

        public_key = private_key.publickey()
        public_key_pem = public_key.export_key()
        utf8_public_key = public_key_pem.decode('utf8')

        return utf8_private_key, utf8_public_key

class UserConfig(congo_models.AbstractUserConfig):
    pass

