# -*- coding: utf-8 -*-
import mimetypes
import os

from .settings import *

mimetypes.add_type("image/svg+xml", ".svg", True)

DEBUG = True
# DEBUG = False

DATABASES['default']['HOST'] = '127.0.0.1'

ALLOWED_HOSTS = (
    'tpass.test',
    'localhost',
)

# Debug Toolbar

INTERNAL_IPS = ('127.0.0.1',)

# E-mail

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_BACKEND = 'congo.communication.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'temp', 'outbox')

# Congo

# Communication

CONGO_EMAIL_PREMAILER_BASE_PATH = os.path.join(BASE_DIR, 'core')
