"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from rest_framework import routers

from accounts.api_views import UserViewSet
from crypto import views as crypto
from security.api_views import PasswordGroupViewSet
from security.api_views import UserPasswordViewSet

router = routers.SimpleRouter()

router.register(r'users', UserViewSet)
router.register(r'groups', PasswordGroupViewSet)
router.register(r'passwords', UserPasswordViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

api_urls = ([
    # router
    path('', include(router.urls)),
], 'api')

crypto_urls = ([
    path('gen-keys/', crypto.gen_keys, name = 'gen_keys'),
    path('encrypt/', crypto.encrypt, name = 'encrypt'),
    path('decrypt/', crypto.decrypt, name = 'decrypt'),
], 'crypto')

urlpatterns += [
    path('api/v1/', include(api_urls, namespace = 'api')),
    path('crypto/', include(crypto_urls, namespace = 'crypto')),
]
