# -*- coding: utf-8 -*-
from .local_settings import *

DATABASES['default']['HOST'] = '127.0.0.1'
DATABASES['default']['NAME'] = 'tpass1_model'
