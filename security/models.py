from django.db import models
from mptt.fields import TreeForeignKey
from mptt.managers import TreeManager
from mptt.models import MPTTModel

from accounts.models import User

class PasswordGroup(MPTTModel):
	owner = models.ForeignKey(User, verbose_name = "Category owner", related_name = "user_categories", on_delete = models.CASCADE)
	parent = TreeForeignKey('self', verbose_name = "Parent group", related_name = 'group_children', blank = True, null = True, on_delete = models.CASCADE)
	name = models.TextField("Name")

	objects = TreeManager()

	class Meta:
		verbose_name = "Password group"
		verbose_name_plural = "Password groups"

	def __str__(self):
		return self.name


class Password(models.Model):
	group = models.ForeignKey(PasswordGroup, verbose_name = "Group", related_name = "group_passwords", on_delete = models.CASCADE)

	class Meta:
		verbose_name = "Password"
		verbose_name_plural = "Passwords"


class UserPassword(models.Model):
	user = models.ForeignKey(User, verbose_name = "User", related_name = "user_user_passwords", on_delete = models.CASCADE)
	password = models.ForeignKey(Password, verbose_name = "Password", related_name = "password_users_passwords", on_delete = models.CASCADE)

	title = models.TextField("Title", blank = True, null = True)
	user_name = models.TextField("User name", blank = True, null = True)
	secret = models.TextField("Secret", blank = True, null = True)
	url = models.TextField("URL", blank = True, null = True)
	notes = models.TextField("Notes", blank = True, null = True)

	class Meta:
		verbose_name = "User password"
		verbose_name_plural = "User password"
		unique_together = ['user', 'password']

	def __str__(self):
		return "%s (%s)" % (self.title, self.user)


class SharedGroup(models.Model):

	VIEW = 10
	CHANGE = 20

	ROLE_CHOICES = (
		(VIEW, "View"),
		(CHANGE, "Change"),
	)

	group = models.ForeignKey(PasswordGroup, verbose_name = "Group", related_name = "group_shared_groups", on_delete = models.CASCADE)
	user = models.ForeignKey(User, verbose_name = "User", related_name = "user_shared_groups", on_delete = models.CASCADE)
	role = models.IntegerField("Role", choices = ROLE_CHOICES, default = VIEW)

	class Meta:
		verbose_name = "Shared group"
		verbose_name_plural = "Shared groups"
		unique_together = ['group', 'user']

	def __str__(self):
		return "%s - %s" % (self.group, self.user)


class SharedPassword(models.Model):

	VIEW = 10
	CHANGE = 20

	ROLE_CHOICES = (
		(VIEW, "View"),
		(CHANGE, "Change"),
	)

	password = models.ForeignKey(Password, verbose_name = "Password", related_name = "password_shared_passwords", on_delete = models.CASCADE)
	user = models.ForeignKey(User, verbose_name = "User", related_name = "user_shared_passwords", on_delete = models.CASCADE)
	role = models.IntegerField("Role", choices = ROLE_CHOICES, default = VIEW)

	class Meta:
		verbose_name = "Shared group"
		verbose_name_plural = "Shared groups"
		unique_together = ['password', 'user']

	def __str__(self):
		return "%s - %s" % (self.password, self.user)
