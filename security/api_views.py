from django.core.exceptions import ObjectDoesNotExist, SuspiciousOperation
from django.db import transaction
from rest_framework import status, mixins
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK
from rest_framework.viewsets import GenericViewSet

from accounts.models import User
from core.api_views import BaseViewSet
from security.models import UserPassword, PasswordGroup, Password, SharedGroup, SharedPassword
from security.serializers import CreateUserPasswordSerializer
from security.serializers import UserPasswordSerializer, PasswordGroupSerializer, SharedGroupsSerializer, SharedPasswordSerializer


class PasswordGroupViewSet(BaseViewSet, viewsets.ModelViewSet):
    queryset = PasswordGroup.objects.all()
    serializer_class = PasswordGroupSerializer

    def list(self, request, *args, **kwargs):
        result = {}

        user = request.user

        self.queryset = self.filter_queryset(self.get_queryset()).filter(owner = user)

        result['password_group'] = self.serializer_class(self.queryset, many = True).data
        result['shared_password_group'] = SharedGroupsSerializer(user.user_shared_groups.all(), many = True).data
        result['shared_passwords'] = SharedPasswordSerializer(user.user_shared_passwords.all(), many = True).data

        return Response(result, status = status.HTTP_200_OK)

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        result = {}

        data = request.data
        data['owner'] = request.user.id

        # TODO: weryfikowac, czy owner parenta nalezy do usera, lub parent jest userowi podsherowany z upr edycji

        serializer = self.serializer_class(data = data)
        serializer.is_valid(raise_exception = True)
        serializer.save()

        result = serializer.data

        return Response(result, status = status.HTTP_201_CREATED)

    @transaction.atomic
    @action(methods = ['post'], detail = True, url_path = 'share')
    def share(self, request, *args, **kwargs):
        result = {}

        instance = self.get_object()

        data = request.data
        data['group'] = instance.id

        if not request.user == instance.owner:
            # sprawdzamy, czy sharujacy jest ownerem grupy
            raise SuspiciousOperation()

        serializer = SharedGroupsSerializer(data = data)
        serializer.is_valid(raise_exception = True)

        # TODO: weryfikować, czy mam uprawnienia do tego katalogu
        # TODO: weryfikowac, czy rodzic nie jest juz udostepniony

        user = serializer.validated_data['user']
        role = serializer.validated_data['role']

        if request.user.id == user.id:
            # sprawdzamy, czy nie sharujemy samym sobie
            raise SuspiciousOperation()

        shared_group, created = SharedGroup.objects.update_or_create(group = instance, user = user, defaults = {'role': role})

        result = {'message': "Group was shared successfully."}

        return Response(result, status.HTTP_200_OK)

    @transaction.atomic
    @action(methods = ['post'], detail = True, url_path = 'unshare')
    def unshare(self, request, *args, **kwargs):
        result = {}

        instance = self.get_object()

        data = request.data
        user_id = data['user']

        if not request.user == instance.owner:
            # sprawdzamy, czy sharujacy jest ownerem grupy
            raise SuspiciousOperation()

        try:
            SharedGroup.objects.filter(group = instance, user_id = user_id).delete()
        except (ValueError, TypeError):
            raise SuspiciousOperation()

        result = {'message': "Group was unshared successfully."}

        return Response(result, status.HTTP_200_OK)


    @transaction.atomic
    @action(methods = ["post"], detail = True, url_path = 'move')
    def move(self, request, *args, **kwargs):
        result = {}

        data = request.data.copy()

        user = request.user
        data['user'] = user.id

        instance = self.get_object()

        try:
            descendants = instance.get_descendants(include_self = True)
            shared_group_exists = SharedGroup.objects.filter(group__in = descendants, user = user, role = SharedGroup.CHANGE).exists()

            # sprawdzamy, czy jestesmy wlascicielem grupy lub ktos nam ja sharuje
            assert instance.owner == user or shared_group_exists

            # TODO: obsluzyc przenoszenie grupy na root'a
            group_id = data.pop('group')
            group = PasswordGroup.objects.get(id = group_id)

            descendants = group.get_descendants(include_self = True)
            shared_group_exists = SharedGroup.objects.filter(group__in = descendants, user = user, role = SharedGroup.CHANGE).exists()

            # sprawdzamy, czy jestesmy wlascicielem nowej grupy lub ktos nam ja sharuje
            assert group.owner == user or shared_group_exists

            if instance.owner != group.owner:
                # sprawdzamy, czy grupa jest przenoszona do grupy tego samego wlasciciela

                result['message'] = "You cannot move a password to a group that belongs to another user."
                return Response(result, HTTP_400_BAD_REQUEST)

        except (KeyError, ValueError, ObjectDoesNotExist, AssertionError):
            raise SuspiciousOperation()

        instance.parent = group
        instance.save(update_fields = ['parent'])

        result['message'] = "Item was moved successfully."
        return Response(result, HTTP_200_OK)


class UserPasswordViewSet(BaseViewSet, mixins.CreateModelMixin,
    mixins.UpdateModelMixin, mixins.DestroyModelMixin, GenericViewSet):

    queryset = UserPassword.objects.all()
    serializer_class = UserPasswordSerializer

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        data = request.data.copy()

        if 'user' in data:
            # jesli podano user_id, znaczy, ze tworzymy user_password dla istneijacego usera,
            # np. przy sharowaniu grupy
            try:
                user = User.objects.get(id = data['user'])
            except (TypeError, ValueError, ObjectDoesNotExist):
                raise SuspiciousOperation()

        else:
            # jesli usera nie podano, pobieramy aktywnego
            # i dla niego tworzymy nowy user_password
            user = request.user
            data['user'] = user.id

        try:
            # TODO: sprawdzic, czy mam uprawnienia do grupy
            group_id = data.pop('group')
            group = PasswordGroup.objects.get(id = group_id)

        except (KeyError, ValueError, ObjectDoesNotExist) as e:
            raise SuspiciousOperation()

        if 'password' in data:
            # jesli podano password_id, znaczy, ze tworzymy user_password dla istneijacego hasla, np.
            # przy sharowaniu grupy
            try:
                password = Password.objects.filter(group = group).get(id = data['password'])
            except (TypeError, ValueError, ObjectDoesNotExist):
                raise SuspiciousOperation()

        else:
            # jesli password nie istnieje, tworzymy nowe dla danej grupy i
            # dopiero potem tworzymy nowy user_password
            password = Password.objects.create(group = group)
            data['password'] = password.id

        # szyfrujemy kolejne dane
        cipher = user.get_cipher()
        for field in ['title', 'user_name', 'secret', 'url', 'notes']:
            value = data.get(field) or None
            if value:
                data[field] = user.encrypt(value, cipher)
            else:
                data[field] = None

        serializer = CreateUserPasswordSerializer(data = data)
        serializer.is_valid(raise_exception = True)
        serializer.save()

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status = status.HTTP_201_CREATED, headers = headers)

    @transaction.atomic
    @action(methods = ['post'], detail = True, url_path = 'share')
    def share(self, request, *args, **kwargs):
        result = {}

        instance = self.get_object()

        user = request.user
        data = request.data
        data['password'] = instance.password.id

        if not instance.user == user:
            # sprawdzamy, czy jestesmy wlascicielem hasla
            raise SuspiciousOperation()

        if not instance.password.group.owner == user:
            # sprawdzamy, czy jestesmy wlascicielem grupy hasla
            raise SuspiciousOperation()

        serializer = SharedPasswordSerializer(data = data)
        serializer.is_valid(raise_exception = True)

        other_user = serializer.validated_data['user']
        role = serializer.validated_data['role']

        shared_password, created = SharedPassword.objects.update_or_create(password = instance.password, user = other_user, defaults = {'role': role})

        # szyfrujemy kolejne dane
        cipher = other_user.get_cipher()
        user_password_dict = {}
        for field in ['title', 'user_name', 'secret', 'url', 'notes']:
            value = data.get(field) or None
            if value:
                user_password_dict[field] = other_user.encrypt(value, cipher)
            else:
                user_password_dict[field] = None

        user_password, created = UserPassword.objects.update_or_create(user = other_user, password = instance.password, defaults = user_password_dict)

        result = {'message': "Item was shared successfully."}

        return Response(result, status.HTTP_200_OK)

    @transaction.atomic
    @action(methods = ['post'], detail = True, url_path = 'unshare')
    def unshare(self, request, *args, **kwargs):
        result = {}

        instance = self.get_object()

        user = request.user
        data = request.data
        user_id = data['user']

        if not instance.password.group.owner == user:
            # sprawdzamy, czy jestesmy wlascicielem grupy hasla
            raise SuspiciousOperation()

        try:
            SharedPassword.objects.filter(password = instance.password, user_id = user_id).delete()

            descendants = instance.password.group.get_descendants(include_self = True)
            shared_group_exists = SharedGroup.objects.filter(group__in = descendants, user = user).exists()

            if not shared_group_exists:
                # usuwamy user password tylko, gdy jakas grupa w drzewie nie jest udostepniona userowi
                UserPassword.objects.filter(password = instance.password, user_id = user_id).delete()
        except (ValueError, TypeError):
            raise SuspiciousOperation()

        result = {'message': "Item was unshared successfully."}

        return Response(result, status.HTTP_200_OK)

    @action(methods = ["post"], detail = True, url_path = 'move')
    def move(self, request, *args, **kwargs):
        result = {}

        data = request.data.copy()

        user = request.user
        data['user'] = user.id

        instance = self.get_object()

        if not instance.user == user:
            # sprawdzamy, czy jestesmy wlascicielem instancji hasla
            raise SuspiciousOperation()

        password = instance.password

        try:
            descendants = password.group.get_descendants(include_self = True)
            shared_group_exists = SharedGroup.objects.filter(group__in = descendants, user = user, role = SharedGroup.CHANGE).exists()
            shared_password_exists = SharedPassword.objects.filter(password = instance, user = user, role = SharedPassword.CHANGE).exists()

            # sprawdzamy, czy jestesmy wlascicielem grupy hasla lub ktos sharuje nam hasło
            assert password.group.owner == user or shared_password_exists or shared_group_exists

            group_id = data.pop('group')
            group = PasswordGroup.objects.get(id = group_id)

            descendants = group.get_descendants(include_self = True)
            shared_group_exists = SharedGroup.objects.filter(group__in = descendants, user = user, role = SharedGroup.CHANGE).exists()

            # sprawdzamy, czy jestesmy wlascicielem nowej grupy lub ktos nam ja sharuje
            assert group.owner == user or shared_group_exists

            if password.group.owner != group.owner:
                # sprawdzamy, czy haslo przenosimy do grupy tego samego wlasciciela

                result['message'] = "You cannot move a password to a group that belongs to another user."
                return Response(result, HTTP_400_BAD_REQUEST)

        except (KeyError, ValueError, ObjectDoesNotExist, AssertionError):
            raise SuspiciousOperation()

        password.group = group
        password.save(update_fields = ['group'])

        result['message'] = "Item was moved successfully."
        return Response(result, HTTP_200_OK)
