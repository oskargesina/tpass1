# -*- coding: utf-8 -*-
from rest_framework import serializers, fields

from accounts.serializers import UserSerializer
from security.models import PasswordGroup, SharedGroup, SharedPassword, UserPassword

#===============================================================================
# UserPassword
#===============================================================================


class UserPasswordSerializer(serializers.ModelSerializer):

	class Meta:
		model = UserPassword
		fields = (
	        'id',
	        'title',
	        'user_name',
	        'secret',
	        'url',
	        'notes',
	    )


class CreateUserPasswordSerializer(serializers.ModelSerializer):
	group = fields.IntegerField(required = False)

	class Meta:
		model = UserPassword
		fields = (
	        'id',
	        'user',
	        'group',
	        'password',
	        'title',
	        'user_name',
	        'secret',
	        'url',
	        'notes',
	    )


class UpdateGroupUserPasswordSerializer(serializers.ModelSerializer):
	group = fields.IntegerField()

	class Meta:
		model = UserPassword
		fields = (
	        'id',
	        'group',
	    )

#===============================================================================
# PasswordGroup
#===============================================================================


class PasswordGroupSerializer(serializers.ModelSerializer):
	password_objects = serializers.SerializerMethodField()

	class Meta:
		model = PasswordGroup
		fields = (
			'id',
			'parent',
			'owner',
			'name',
			'level',
			'password_objects',
		)

	def get_password_objects(self, obj):
		return UserPasswordSerializer(UserPassword.objects.filter(password__group = obj), many = True).data

#===============================================================================
# SharedGroup
#===============================================================================


class SharedGroupsSerializer(serializers.ModelSerializer):
	groups_objects = serializers.SerializerMethodField()
	owner = serializers.SerializerMethodField()

	class Meta:
		model = SharedGroup
		fields = (
			'owner',
			'role',
      		'user',
        	'group',
			'groups_objects'
		)

	def get_groups_objects(self, obj):
		return PasswordGroupSerializer(obj.group.get_descendants(include_self=True), many=True).data

	def get_owner(self, obj):
		return UserSerializer(obj.group.owner).data

#===============================================================================
# SharedPassword
#===============================================================================


class SharedPasswordSerializer(serializers.ModelSerializer):
	password_object = serializers.SerializerMethodField()
	owner = serializers.SerializerMethodField()

	title = serializers.CharField(allow_blank = True, required = False)
	user_name = serializers.CharField(allow_blank = True, required = False)
	secret = serializers.CharField(allow_blank = True, required = False)
	url = serializers.CharField(allow_blank = True, required = False)
	notes = serializers.CharField(allow_blank = True, required = False)

	class Meta:
		model = SharedPassword
		fields = (
			'owner',
			'role',
      		'user',
        	'password',
			'password_object',

	        'title',
	        'user_name',
	        'secret',
	        'url',
	        'notes',
		)

	def get_password_object(self, obj):
		try:
			users_password = obj.password.password_users_passwords.get(user = obj.user)
		except UserPassword.DoesNotExist:
			# TODO: taka sytuacja nie powinna wystaic!
			users_password = UserPassword(user = obj.user, password = obj.password)

		return UserPasswordSerializer(users_password).data

	def get_owner(self, obj):
		return UserSerializer(obj.password.group.owner).data
