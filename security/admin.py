

from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from core.admin import ModelAdmin
from security.models import PasswordGroup, UserPassword, Password


class PasswordGroupAdmin(DraggableMPTTAdmin):
    pass


class PasswordAdmin(ModelAdmin):
    pass


class UserPasswordAdmin(ModelAdmin):
    pass


admin.site.register(PasswordGroup, PasswordGroupAdmin)
admin.site.register(Password, PasswordAdmin)
admin.site.register(UserPassword, UserPasswordAdmin)
